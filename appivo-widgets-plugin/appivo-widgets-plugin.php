<?php
/*
Plugin Name: Appivo Data Widgets 
Plugin URI: https://apps.appivo.com/appivo-plugins/
Description: Plugin that will create a custom post type displaying data using appivo widgets
Version: 1.0.0
Author: Parv Kothari
*/

define( 'AP_WIDGET_TEMPLATES_PATH' , plugin_dir_path( __FILE__ ). 'templates/' );
define( 'AP_WIDGET_INCLUDES_PATH' , plugin_dir_path( __FILE__ ). 'includes/' );
define( 'AP_WIDGET_ASSETS_PATH' , plugin_dir_path( __FILE__ ). 'assets/' );
define( 'PLUGIN_BASE_NAME', plugin_basename( __FILE__ ));

/**
 * Begins execution of the plugin.
 *
 * Init the plugin process
 *
 * @since    1.0.0
 */

/**
 * The core plugin class
 */
require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widget-admin.php';
require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widget-form.php';

add_action( 'init', 'appivo_widgets_plugin_init' );

function appivo_enqueue_clientjs() {
    
    //register client side js file
    wp_register_script( 'appivoclientjs', plugins_url( '/assets/js/appivoclientjs.js', __FILE__) , array( 'jquery' ) , '1.0.0' , true );
    wp_enqueue_script( 'appivoclientjs' );
    
    //register moment js
    wp_register_script( 'appivomomentjs', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js', null, null, true );
    wp_enqueue_script('appivomomentjs');    

    //register handlebars js
    wp_register_script( 'appivohandlebars', 'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.12/handlebars.min.js', null, null, true );
    wp_enqueue_script('appivohandlebars');
}
  
add_action( 'wp_enqueue_scripts', 'appivo_enqueue_clientjs' );


function appivo_widgets_plugin_init() {
    register_post_type( 'appivo_widgets',
        array(
            'menu_icon' => 'dashicons-portfolio',
            'labels' => array(
                'name' => 'Appivo Widgets',
                'singular_name' => 'Appivo View',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New View',
                'edit' => 'Edit',
                'edit_item' => 'Edit View',
                'new_item' => 'New View',
                'view' => 'View',
                'view_item' => 'View',
                'search_items' => 'Search an appivo view',
                'not_found' => 'No views found',
                'not_found_in_trash' => 'No views found in Trash',
                'parent' => ''
            ),
            'rewrite' => false,
            'query_var' => false,
            'public' => false,
            'capability_type' => 'page',
        )
    );

    $appivo_api = new Appivo_Widget_Admin();
    flush_rewrite_rules();
    
}

function apdw_preprocesstemplate($postId = null) {
    global $wp_query, $post;

    if ( ! class_exists( 'Appivo_Widget_Display' ) ) {
        require_once AP_WIDGET_TEMPLATES_PATH.'appivowidgetdisplay.php';
    }

    if (! empty($postId)) {
        $postToProcess = get_post((int)$postId);
        $postToProcessMeta = get_post_meta( (int) $postId);
    }
    else {
        $postToProcess = $post;
    }

    $instance = array(
        'appname' => $postToProcess->appname,
        'modelname' => $postToProcess->modelname,
        'queryname' => $postToProcess->queryname,
        'viewType' => $postToProcess->viewType,
        'queryParams' => $postToProcess->queryParams,
        'template_type' => $postToProcess->template_type,
        'customTemplate' => $postToProcess->post_content,
        'appivo_isDefaultTemplate' => $postToProcess->appivo_isDefaultTemplate,
        'singleViewParams' => $postToProcess->singleViewParams,
        'singleViewRecordRelations'  => $postToProcess->singleViewRecordRelations
    );

    $appivoDisplayCls =  new Appivo_Widget_Display();
    return $appivoDisplayCls->getWidgetHtml($instance);
}

function apdw_runshortcode($atts, $content = null, $code = '') {
    extract(shortcode_atts(array(
      'id' => -1,
      'post_title' => '',
    ), $atts));

    //Check and run the method only for appivo data widget shortcodes
    if ($code === 'appivo-data-widget') {
        return apdw_preprocesstemplate($id);
    }
    
}

add_shortcode('appivo-data-widget', 'apdw_runshortcode');

?>