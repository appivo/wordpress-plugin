
var appivo_widget_data_result;
var appivo_widget_meta_data;
var appivo_widget_template;

var appivoHandleBarhelpers = {
	'formatDate' : function(dateToFormat, format) {
		if (dateToFormat) {
			var dateToReturn = new Date(dateToFormat);
			return moment(dateToReturn).format(format);
		}
		return '';
	},

	'getSiteUrl': function(){
		var metadata = JSON.parse(appivo_widget_meta_data);
		return metadata.wordpressUrl;
	}

}

jQuery(document).ready(function() {

	jQuery(".appivo-widget-html").ready(function(event) {
		if(appivo_widget_data_result) {
			var appivoWidgetHtml = jQuery(".appivo-widget-html")[0];
			var templateInnerHtml = appivo_widget_template;

			var options = {
				data: appivo_widget_data_result
			}

			//register all the default helpers
			registerAppivoHandlebarsHelpers();

			//compile the template
			templateToAppend = Handlebars.compile(templateInnerHtml);
			
			//assign data
			var htmlToAppend = templateToAppend(appivo_widget_data_result);

			appivoWidgetHtml.innerHTML = '';
			appivoWidgetHtml.innerHTML = htmlToAppend;	
		}
	});
});

function registerAppivoHandlebarsHelpers(){

	for (helper in appivoHandleBarhelpers) {
		Handlebars.registerHelper(helper, appivoHandleBarhelpers[helper]);
	}
}