jQuery(document).ready(function() {
	
	jQuery(document).on('widget-updated', function(event, widget) {
		
	});

	jQuery(document).on('widget-added', function(event, widget) {
	});

	jQuery( "#apdw-form" ).submit(function( event ) {
	  apdwOnWidgetFormSubmit();
	});
});

function defaultTemplateCheckBoxHandler(ctr) {
	var defaultTemplateContainer = jQuery('.appivodefaulttemplatecontainer');
	var customTemplateContainer = jQuery('.appivocustomtemplatecontainer');
	defaultTemplateContainer.toggle();
	customTemplateContainer.toggle();
}

function onViewTypeChange(ctr) {
	var appivoQueryViewContainer = jQuery('.appivoqueryviewcontainer');
	var appivoSingleViewContainer = jQuery('.appivosingleviewcontainer');

	if (ctr.value == 'queryView') {
		appivoQueryViewContainer.show();
		appivoSingleViewContainer.hide();
	}

	if (ctr.value == 'singleView') {
		appivoSingleViewContainer.show();
		appivoQueryViewContainer.hide();
	}
}

function apdwOnWidgetFormSubmit() {
	var form = jQuery('#apdw-form');

	var viewType = form.find('[name="viewType"]')[0] ? form.find('[name="viewType"]')[0].value : '';

	switch (viewType) {
		case 'singleView':

			var idSourceField = jQuery('.tblsingleviewparams').find('[name="singleIdSource"]')[0];
			var idValueField = jQuery('.tblsingleviewparams').find('[name="singleIdSourceValue"]')[0];

			if (idValueField.value && idValueField.value.length > 0) {
				var objIdSource = {
					'key': 'id',
					'value': idValueField.value,
					'valuesource' : idSourceField.value
				}

				jQuery('[name="singleViewParams"]')[0].value = JSON.stringify(objIdSource);
			}
			else {
				window.alert('Please provide id source and value');
				return false;
			}

			break;


		case 'queryView':
			var queryParams = [];
			var tableQueryParams = jQuery('#tblqueryparams');
			jQuery('.tblqueryparams').find('tr').each(function(index) {
				var paramkey = this.getElementsByClassName('keyfield')[0] ? this.getElementsByClassName('keyfield')[0].value : '';
				var paramvalue = this.getElementsByClassName('valuefield')[0] ? this.getElementsByClassName('valuefield')[0].value : '';
				var paramvalueSource = this.getElementsByClassName('valuesource')[0] ? this.getElementsByClassName('valuesource')[0].value : '';

				if (paramkey && paramkey.length > 0) {
					queryParams.push({
						'key': paramkey,
						'value': paramvalue,
						'valuesource': paramvalueSource
					});
				}
			});

			var hiddenqueryparamfld = jQuery('.tblqueryparams').closest('form').find('.hiddenqueryparam')[0];
			hiddenqueryparamfld.value = queryParams && queryParams.length > 0 ? JSON.stringify(queryParams) : '';
			break;

		default:

			window.alert('No view type set');
			return false;

	}
}

function onAddQueryParam(ctr) {
	if (ctr && ctr.type == 'click') {
		ctr = this;
	}
	addNewRowHtml(jQuery('.tblqueryparams > tbody')[0]);
}

function onRemoveQueryParam(ctr) {

	if (ctr && ctr.type == 'click') {
		ctr = this;
	}

	try {
		var hiddenqueryparamfld = jQuery(ctr).closest('form').find('.hiddenqueryparam')[0];
		var keyExists = false;
		if (hiddenqueryparamfld.value.length > 0) {
			var valueHiddenParamArray = JSON.parse(hiddenqueryparamfld.value);
		}

		var trRow = ctr.closest('tr');
		var paramkey = trRow.getElementsByClassName('keyfield')[0] ? trRow.getElementsByClassName('keyfield')[0].value : '';
		var paramvalue = trRow.getElementsByClassName('valuefield')[0] ? trRow.getElementsByClassName('valuefield')[0].value : '';
		var paramvalueSource = trRow.getElementsByClassName('valuesource')[0] ? trRow.getElementsByClassName('valuesource')[0].value : '';

		var tableRecords = jQuery(ctr).closest('table>tbody');

		if (tableRecords.find('tr').length == 1) {
			//If last row - then clear only elements
			trRow.getElementsByClassName('keyfield')[0].value = '';
			trRow.getElementsByClassName('valuefield')[0].value = '';
			trRow.getElementsByClassName('valuesource')[0].value = 'userInput';

			hiddenqueryparamfld.value = '';

		} else {
			//Remove row and modify resulting json
			if (valueHiddenParamArray && valueHiddenParamArray instanceof Array) {

				//Check if key already exists
				valueHiddenParamArray.forEach(function(item, index) {
					if (paramkey == item.key) {
						valueHiddenParamArray.splice(index, 1);
						jQuery(ctr).closest('tr')[0].remove();
						return;
					}
				});
				hiddenqueryparamfld.value = valueHiddenParamArray.length > 0 ? JSON.stringify(valueHiddenParamArray) : '';
			}
			else {
				jQuery(ctr).closest('tr')[0].remove();
			}
		}

		if (jQuery(ctr).closest('form').find('[name="savewidget"]')[0]) { jQuery(ctr).closest('form').find('[name="savewidget"]')[0].disabled = false; }
	} catch (err) {
		window.alert('Error Occured: ' + err.message);
	}
}

function addNewRowHtml(tablebody, objValue) {
	var newrow = tablebody.insertRow(-1);

	//--------------- Key field -----------------------------

	var keycell = newrow.insertCell(0);
	var inputCellKey = document.createElement("input");
	inputCellKey.type = "text";
	inputCellKey.className = "keyfield";
	inputCellKey.placeholder = "Parameter Key";
	if (objValue && objValue.key) {
		inputCellKey.value = objValue.key;
	}
	keycell.appendChild(inputCellKey);

	//--------------- Select field ---------------------------------

	var valuesourcecell = newrow.insertCell(1);
	var inputCellValueSource = document.createElement("select");
	inputCellValueSource.className = "valuesource";

	inputCellValueSource.options.add(new Option('Fixed', 'userInput'));
	inputCellValueSource.options.add(new Option('Query Parameter', 'url'));
	inputCellValueSource.options.add(new Option('Cookie', 'cookie'));

	if (objValue && objValue.value) {
		inputCellValueSource.value = objValue.value;
	}

	valuesourcecell.appendChild(inputCellValueSource);


	//----------------- Value field ---------------------------------------

	var valuecell = newrow.insertCell(2);
	var inputCellValue = document.createElement("input");
	inputCellValue.type = "text";
	inputCellValue.className = "valuefield";
	inputCellValue.placeholder = "Parameter Value";
	if (objValue && objValue.value) {
		inputElCell1.value = objValue.valuesource;
	}
	valuecell.appendChild(inputCellValue);

	//--------------- Button field -------------------------------

	var rembtncell = newrow.insertCell(3);
	var inputElCell5 = document.createElement("input");
	inputElCell5.type = "button";
	inputElCell5.value = "Remove";
	rembtncell.appendChild(inputElCell5);
	inputElCell5.onclick = this.onRemoveQueryParam;

}