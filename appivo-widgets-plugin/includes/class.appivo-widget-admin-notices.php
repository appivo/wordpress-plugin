<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Appivo_Widget_Admin_Notices{

    /**
     * Holds an array of notices to be displayed
     * @var [type]
     */
    private $notices;

    /**
     * The main class construcator
     */
    public function __construct(){

        $this->register_hooks();

    }

    /**
     * Registers class filters and actions
     * @return [null]
     */
    public function register_hooks(){
        /**
         * display notices hook
         */
        add_action( 'admin_notices' , array( $this , 'apdw_admin_notices' ) );
    }

    /**
     * display the notices that resides in the notices collection
     * @return [type] [description]
     */
    public function apdw_admin_notices(){

        if( $this->notices ){
            foreach( $this->notices as $admin_notice ){
                /**
                 * only disply the notice if it wasnt dismiised in the past
                 */
                $classes = array(
                    "notice notice-{$admin_notice['type']}",
                    "is-dismissible"
                );

                $id = $admin_notice['id'];
                if( ! $admin_notice['dismissable_forever'] || (! isset( $this->notices_options['dismiss_notices'][$id] ) || ! $this->notices_options['dismiss_notices'][$id]) ){
                    if( $admin_notice['dismissable_forever'] ){
                        $classes[] = 'qs-cf7-api-dismiss-notice-forever';
                    }
                    echo "<div id='{$admin_notice['id']}' class='".implode( ' ' , $classes )."'>
                         <p>{$admin_notice['notice']}</p>
                     </div>";
                }

            }
        }
    }

    /**
     * adds notices to the class notices collection
     * @param array $notice an array of notice message and notice type
     * Types available are "error" "warning" "success" "info"
     */
    public function wp_add_notice( $notice = "" ){

        if( $notice ){
            $this->notices[] = array(
                'id'     => $notice['id'],
                'notice' => $notice['notice'],
                'type'   => isset( $notice['type'] ) ? $notice['type'] : 'warning',
                'dismissable_forever' => isset( $notice['dismissable_forever'] ) ? $notice['dismissable_forever'] : false
            );
        }

    }
}
