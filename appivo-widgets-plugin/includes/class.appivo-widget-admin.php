<?php


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widget-post-functions.php';
require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widgets-list-table.php';
require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widget-admin-notices.php';


class Appivo_Widget_Admin{

    /**
     * Holds the plugin options
     * @var [type]
     */
    public $options;

    /**
     * Holds the plugin options
     * @var [type]
     */
    public $post_type;

    /**
     * Holds athe admin notices class
     * @var [Appivo_Widget_Admin_Notices]
     */
    private $admin_notices;

    /**
     * PLugn is active or not
     */
    private $plugin_active;
	/**
	 * API errors array
	 * @var [type]
	 */
	private $api_errors;

    public function __construct(){

        $this->getOptionsToPrefill();
        $this->register_hooks();
        $this->admin_notices = new Appivo_Widget_Admin_Notices();

    }

    private function getOptionsToPrefill(){
        $this->options = get_option('appivo_options');
    }

    public function verify_dependencies() {


        //Verify contact form 7 works..
        if( ! is_plugin_active('contact-form-7/wp-contact-form-7.php') ){
            $notice = array(
                'id'                  => 'cf7-not-active',
                'type'                => 'warning',
                'notice'              => 'Appivo widget requires Contact Form 7 Plugin to be installed and active',
                'dismissable_forever' => false
            );

            $this->admin_notices->wp_add_notice( $notice );
        }

        //Verify user has set authentication options
        $options = $this->options;
        $isAuthenticationOptionsSet = false;

        if (isset($options) && isset($options['user_access_token'])) {
            $isAuthenticationOptionsSet = true;
        }

        if (!$isAuthenticationOptionsSet){
            $notice = array(
                'id'                  => 'auth-options-not-present',
                'type'                => 'error',
                'notice'              => 'Please add an Access-token to enable integration with your Appivo App. Consider contacting your Administrator to retrieve an access-token. <a href="options-general.php?page=appivo_plugin">Go to Settings</a>',
                'dismissable_forever' => false
            );

            $this->admin_notices->wp_add_notice( $notice );
        }
    }

    public function admin_page_initialize()
    {        
        
        $this->post_type = 'appivo_widgets';
        self::verify_dependencies();
        self::add_authentication_section();    
    }

    function add_appivo_widget_plugin_metabox() {
        add_meta_box( 'appivo_widget_metabox', 'Appivo Widget', array($this, 'display_appivo_widget_settings'), 'appivo_widgets', 'normal', 'high');
    }

    function display_appivo_widget_settings($post_id = null) {
        $instance = array();
        if (isset($post_id) && $post_id > 0) {

            $postData = get_post($post_id);
            $postMetaData = get_post_meta( $post_id, '', true);
            $instance['post_title'] = $postData->post_title;
            $instance['post_id'] = $post_id;
            $instance['customTemplate'] = $postData->post_content;
            $instance['appname'] = $postData->appname;
            $instance['modelname'] = $postData->modelname;
            $instance['viewType'] = $postData->viewType;
            $instance['queryname'] = $postData->queryname;
            $instance['queryParams'] = $postData->queryParams;
            $instance['template_type'] = $postData->template_type;
            $instance['appivo_isDefaultTemplate'] = $postData->appivo_isDefaultTemplate;
            $instance['singleViewParams'] = $postData->singleViewParams;
            $instance['singleViewRecordRelations'] = $postData->singleViewRecordRelations;

        }

        if (! empty($postData->singleViewParams)) {
            $singleParamArray = json_decode($postData->singleViewParams);
            $instance['singleIdSource'] = $singleParamArray->valuesource;
            $instance['singleIdSourceValue'] = $singleParamArray->value;
        }

        // Set widget defaults
        $defaults = array(
            'post_id' => -1,
            'post_title' => '',
            'appname'    => '',
            'modelname'  => '',

            'viewType' => 'singleView',

            'singleViewParams' => '',

            'queryname' => '',
            'queryParams' => '',
            'template_type' => 'list',
            'customTemplate' => '',
            'appivo_isDefaultTemplate' => false,
            'singleViewParams' => '',
            'singleIdSource' => 'userInput',
            'singleIdSourceValue' => '',
            'singleViewRecordRelations' => ''
        );
        
        // Parse current settings with defaults
        extract( wp_parse_args( ( array ) $instance, $defaults ) );

        ?>
        
        <form action="<?php echo esc_url( add_query_arg( array( 'post' => $post_id ), menu_page_url( 'apdw', false ) ) ); ?>" method="post" id="apdw-form">

            <?php wp_nonce_field( 'apdw-save-widget-form_' . $post_id ); ?>

            <input type="hidden" id="hiddenaction" name="action" value="save" />
            <input type="hidden" id="post_id" name="post_id" value="<?php echo (int) $post_id; ?>" />

            <?php //AppName ?>
            <p>         
                Widget Name: <input class="" id="post_title" name="post_title" type="text" value="<?php echo esc_attr( $post_title ); ?>" required/>
            </p>

            <?php //AppName ?>
            <p>         
                App Name: <input class="" id="appname" name="appname" type="text" value="<?php echo esc_attr( $appname ); ?>" />
            </p>

            <?php // Model Name ?>
            <p>
                <?php _e( 'Model Name: ', 'text_domain' ); ?><input class="" id="modelname" name="modelname" type="text" value="<?php echo esc_attr( $modelname ); ?>" />
            </p>    

            <!--Dropdown-->
            <p>
                <label for="viewType"><?php _e( 'View Type:', 'text_domain' ); ?></label>
                <select name="viewType" id="viewType" class="" onchange="onViewTypeChange(this)">
                <?php
                // Your options array
                $options = array(
                    'queryView' => __( 'List Record View', 'text_domain' ),
                    'singleView' => __( 'Single Record View', 'text_domain' )
                );

                // Loop through options and add each one to the select dropdown
                foreach ( $options as $key => $name ) {
                    echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $viewType, $key, false ) . '>'. $name . '</option>';

                }?>
                </select>
            </p>

            <div id="appivoqueryviewcontainer" class="appivoqueryviewcontainer" style="display: <?php if($viewType == 'queryView') {echo 'block';} else {echo 'none';} ?>">
                <h3>List View Configurations:</h3>

                <?php // Query Name ?>
                <p>
                    
                    Query Name: <input class="" id="queryname" name="queryname" type="text" value="<?php echo esc_attr( $queryname ); ?>" />
                </p>

                Query Parameters:
                <table class="tblqueryparams" data-ischanged="false" style="margin-top: 5px;">
                    <tbody class="tblbodyqueryparams">
                        <?php $this->getParamRowsHtml($queryParams); ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <input type="button" name="" onclick="onAddQueryParam(this)" value="Add New">
                            </td>
                        </tr>
                    </tfoot>
                </table>

                <input type="text" name="queryParams" class="hiddenqueryparam" style="display:none;" value="<?php echo esc_attr( $queryParams ); ?>" />
            </div>

            <div id="appivosingleviewcontainer" class="appivosingleviewcontainer" style="display: <?php if($viewType == 'singleView') {echo 'block';} else {echo 'none';} ?>">
                <h3>Single Record View Configurations:</h3>
                <p>
                    <table class="tblsingleviewparams" data-ischanged="false">
                        <tbody class="tblIdParam">
                            <tr class="">
                                <td>Pick ID From: </td>
                                <td>
                                    <select name='singleIdSource' class='valuesource' value="<?php echo esc_attr( $singleIdSource ); ?>" >
                                        <option value='userInput' <?php if($singleIdSource == 'userInput'){echo("selected");}?> >Fixed</option>
                                        <option value='url' <?php if($singleIdSource == 'url'){echo("selected");}?> >Query Parameter</option>
                                        <option value='cookie' <?php if($singleIdSource == 'cookie'){echo("selected");}?> >Cookie</option>
                                    </select>
                                </td>
                                <td>
                                    <input type='text' name='singleIdSourceValue' class='valuefield' placeholder='Parameter Value' value="<?php echo esc_attr( $singleIdSourceValue ); ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Relations: 
                                    <span>(Comma seperated)</span>
                                </td>
                                <td>
                                    <input type='text' name='singleViewRecordRelations' class='valuefield' placeholder='ex. relation1, relation2' value="<?php echo esc_attr( $singleViewRecordRelations ); ?>">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </p>
                
                <input type="text" name="singleViewParams" class="hiddenqueryparam" style="display:none;" value="<?php echo esc_attr( $singleViewParams ); ?>" />    
            
            </div>

            <h3>Template: </h3>

            <?php // Checkbox ?>
            <p style="display: none;">
                <input id="appivo_isDefaultTemplate" name="appivo_isDefaultTemplate" type="checkbox" value="1" click="defaultTemplateCheckBoxHandler(this)" <?php checked( '1', $appivo_isDefaultTemplate ); ?> />
                <label for="appivo_isDefaultTemplate"><?php _e( 'Default Template?', 'text_domain' ); ?></label>
            </p>

            <div>
                <div id = "appivodefaulttemplatecontainer" class="appivodefaulttemplatecontainer" style="display:none;">
                    <?php // Dropdown ?>
                    <p>
                        <?php _e( 'Default Template:', 'text_domain' ); ?>
                        <select name="template_type" id="template_type" class="">
                        <?php
                        // Your options array
                        $options = array(
                            'grid' => __( 'Grid View', 'text_domain' ),
                            'list' => __( 'List View', 'text_domain' )
                        );

                        // Loop through options and add each one to the select dropdown
                        foreach ( $options as $key => $name ) {
                            echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $template_type, $key, false ) . '>'. $name . '</option>';

                        } ?>
                        </select>
                    </p>
                </div>
                <div id = "appivocustomtemplatecontainer" class="appivocustomtemplatecontainer" style="display: block ?>;">
                    <label for="customTemplate"><?php _e( 'Enter Custom Template:', 'text_domain' ); ?></label>
                    <textarea class="widefat" cols="40" rows="20" id="customTemplate" name="customTemplate"><?php echo wp_kses_post( $customTemplate ); ?></textarea>
                </div>
            </div>

            <input name="save" type="submit" class="button button-primary button-large" id="publish" value="Publish" />
        </form>

        <?php
    }

    /**
     * Registers the required admin hooks
     * @return [type] [description]
     */
    public function register_hooks(){
        add_action( 'admin_init', array( $this, 'admin_page_initialize' ));
        //add_action( 'add_meta_boxes', array( $this, 'add_appivo_widget_plugin_metabox') );

        add_action( 'admin_menu', array($this, 'admin_menu_create' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'appivo_widgets_plugin_loadadminscripts') );

        add_action( 'wpcf7_before_send_mail', 'apdw_cf7_before_send_mail');

        $pluginfiltername = 'plugin_action_links_'.PLUGIN_BASE_NAME;
        add_filter( $pluginfiltername, array($this, 'add_appivo_settings_link'));

    }

    function admin_menu_create() {
        global $_wp_last_object_menu;

        $_wp_last_object_menu++;


        /*Authentication section in settings*/
        add_options_page( 'Appivo Settings', 'Appivo', 'manage_options', 'appivo_plugin', array($this, 'add_plugin_display_page'), $icon_url = '', 110 );

        /*Custom widgets list and other pages*/

        add_menu_page( __( 'Appivo Widgets', 'contact-form-7' ),
            __( 'Appivo Widget', 'contact-form-7' ),
            'manage_options', 'apdw',
            array($this, 'addAdminManagementPage'), 'dashicons-email',
            $_wp_last_object_menu );

        $edit = add_submenu_page( 'apdw',
        __( 'Edit Appivo Widget', 'contact-form-7' ),
        __( 'Appivo Widgets', 'contact-form-7' ),
        'manage_options', 'apdw',
        array($this, 'addAdminManagementPage') );

        add_action( 'load-' . $edit, array($this,'apdw_load_appivo_widget_admin') );

        $addnew = add_submenu_page( 'apdw', 
            'Add New Widget', 
            'New Widget', 
            'manage_options', 'apdw-new',
            array($this, 'display_appivo_widget_settings'));

        add_action( 'load-' . $addnew, array($this,'apdw_load_appivo_widget_admin') );
    }

    function apdw_load_appivo_widget_admin() {
        global $plugin_page;

        $action = $this->apdw_current_action();

        if ( 'save' == $action ) {

            $id = isset( $_POST['post_id'] ) ? $_POST['post_id'] : '-1';
            check_admin_referer( 'apdw-save-widget-form_' . $id );

            // if ( ! current_user_can( 'wpcf7_edit_contact_form', $id ) ) {
            //     wp_die( __( 'You are not allowed to edit this item.', 'contact-form-7' ) );
            // }

            $args = $_REQUEST;
            $args['id'] = $id;

            $args['post_title'] = isset( $_POST['post_title'] )
                ? $_POST['post_title'] : '';

            $args['appname'] = isset( $_POST['appname'] )
                ? $_POST['appname'] : '';

            $args['modelname'] = isset( $_POST['modelname'] )
                ? $_POST['modelname'] : '';

            $args['viewType'] = isset( $_POST['viewType'] )
                ? $_POST['viewType'] : '';

            $args['queryname'] = isset( $_POST['queryname'] )
                ? $_POST['queryname'] : '';

            $args['queryParams'] = isset( $_POST['queryParams'] )
                ? $_POST['queryParams'] : array();

            $args['appivo_isDefaultTemplate'] = isset( $_POST['appivo_isDefaultTemplate'] )
                ? $_POST['appivo_isDefaultTemplate'] : true;

            $args['customTemplate'] = isset( $_POST['customTemplate'] )
                ? $_POST['customTemplate'] : '';

            $args['singleViewParams'] = isset( $_POST['singleViewParams'] )
                ? $_POST['singleViewParams'] : '';

            $args['singleViewRecordRelations'] = isset( $_POST['singleViewRecordRelations'] )
                ? $_POST['singleViewRecordRelations'] : '';

            $widget_form = apdw_save_contact_form( $args );

            if ( ! $widget_form ) {
                $query['message'] = 'failed';
            } elseif ( -1 == $id ) {
                $query['message'] = 'created';
            } else {
                $query['message'] = 'saved';
            }

            $redirect_to = add_query_arg( $query, menu_page_url( 'apdw', false ) );
            wp_safe_redirect( $redirect_to );
            exit();
        }

        if ( 'delete' == $action ) {

            check_admin_referer( 'bulk-posts' );

            $posts = empty( $_POST['post_ID'] )
                ? (array) $_REQUEST['post']
                : (array) $_POST['post_ID'];

            $deleted = 0;

            foreach ( $posts as $post ) {
                $post = Appivo_Widget_Form::get_instance( $post );

                if ( empty( $post ) ) {
                    continue;
                }

                if ( ! $post->delete() ) {
                    wp_die( 'Error in deleting.');
                }

                $deleted += 1;
            }

            $query = array();

            if ( ! empty( $deleted ) ) {
                $query['message'] = 'deleted';
            }

            $redirect_to = add_query_arg( $query, menu_page_url( 'apdw', false ) );

            wp_safe_redirect( $redirect_to );
            exit();
        }

        $_GET['post'] = isset( $_GET['post'] ) ? $_GET['post'] : '';

        $post = null;
        $postId = $_GET['post'];

        $current_screen = get_current_screen();

        if ( $post) {
            //$help_tabs->set_help_tabs( 'edit' );
        } else {
            //$help_tabs->set_help_tabs( 'list' );

            if ( ! class_exists( 'APDW_List_Table' ) ) {
                require_once AP_WIDGET_INCLUDES_PATH . 'class.appivo-widgets-list-table.php';;
            }
        }
    }

    function addAdminManagementPage() {
        $post_id = $_GET['post'];
        if ( isset($post_id) && $post_id >= 0 && $post_id != '') {
            $this->display_appivo_widget_settings($post_id);
            return;
        }

        $list_table = new APDW_List_Table();
        $list_table->prepare_items();
        
        ?>
            <div class="wrap">

            <h1 class="wp-heading-inline">Appivo Widgets</h1>

            <?php
                if ( current_user_can( 'wpcf7_edit_contact_forms' ) ) {
                    echo sprintf( '<a href="%1$s" class="add-new-h2">%2$s</a>',
                        esc_url( menu_page_url( 'apdw-new', false ) ),
                        esc_html( __( 'Add New', 'contact-form-7' ) ) );
                }

                if ( ! empty( $_REQUEST['s'] ) ) {
                    echo sprintf( '<span class="subtitle">'
                        /* translators: %s: search keywords */
                        . __( 'Search results for &#8220;%s&#8221;', 'contact-form-7' )
                        . '</span>', esc_html( $_REQUEST['s'] ) );
                }
            ?>

            <hr class="wp-header-end">

            <form method="get" action="">
                <input type="hidden" name="page" value="<?php echo esc_attr( $_REQUEST['page'] ); ?>" />
                <?php $list_table->display(); ?>
            </form>

            </div>
        <?php
    }

    function afterPostSave($post_id, $post) {
        if ( $post->post_type == 'appivo_widgets' ) {
        // Store data in post meta table if present in post data
            if ( isset( $_POST['title'] ) && $_POST['title'] != '' ) {
                update_post_meta( $post_id, 'SampleMetaDataKey', 'SampleMetaDataValue' );
            }
            
        }
    }

    function appivo_widgets_plugin_loadadminscripts( $hook_suffix ) {
        wp_register_script( 'appivowidget', plugins_url( '/assets/js/appivowidget.js', dirname(__FILE__)) , array( 'jquery' ) , '1.0.0' , true );
        wp_enqueue_script( 'appivowidget' );

    }

    function apdw_current_action() {
        if ( isset( $_REQUEST['action'] ) && -1 != $_REQUEST['action'] ) {
            return $_REQUEST['action'];
        }

        if ( isset( $_REQUEST['action2'] ) && -1 != $_REQUEST['action2'] ) {
            return $_REQUEST['action2'];
        }

        return false;
    }


    function getParamRowsHtml($queryParamsJson) {
        if (strlen($queryParamsJson) > 0){
            $queryParamsArray = json_decode($queryParamsJson);
            if (count($queryParamsArray) > 0) {
                foreach($queryParamsArray as $item) {

                        echo "<tr class=\"appivoparam1\">
                            <td>
                                <input type=\"text\" name=\"keyfield\" class=\"keyfield\" placeholder=\"Parameter Key\" value=\"".$item->key."\">
                            </td>
                            <td>
                                <select name=\"valuesource\" class=\"valuesource\" id=\"\" value=\"".$item->valuesource."\">
                                    <option value=\"userInput\">Fixed</option>
                                    <option value=\"url\">Query Parameter</option>
                                    <option value=\"cookie\">Cookie</option>
                                </select>
                            </td>
                            <td>
                                <input type=\"text\" name=\"valuefield\" class=\"valuefield\"  placeholder=\"Parameter Value\" value=\"".$item->value."\">
                            </td>
                            <td>
                                <input type=\"button\" name=\"\" onclick=\"onRemoveQueryParam(this)\" value=\"Remove\">
                            </td>
                        </tr>";
                    }
                unset($item);
                }
            
        } else {
            echo $this->getTemplateQueryRowHtml();
        }
    }

    function getTemplateQueryRowHtml(){
        return "<tr class=\"appivoparam1\">
                            <td>
                                <input type=\"text\" name=\"keyfield\" class=\"keyfield\" placeholder=\"Parameter Key\">
                            </td>
                            <td>
                                <select name=\"valuesource\" class=\"valuesource\" id=\"\">
                                    <option value=\"userInput\">Fixed</option>
                                    <option value=\"url\">Query Parameter</option>
                                    <option value=\"cookie\">Cookie</option>
                                </select>
                            </td>
                            <td>
                                <input type=\"text\" name=\"valuefield\" class=\"valuefield\" placeholder=\"Parameter Value\">
                            </td>
                            <td>
                                <input type=\"button\" name=\"\" onclick=\"onRemoveQueryParam(this)\" value=\"Remove\">
                            </td>
                        </tr>";
    }

    /********************************* Section of functions to create settings page for key and secret ******************************************/

    public function add_plugin_display_page() {

        ?>
        <div class="wrap">
            <h1>Authentication Settings</h1>
            <form method="post" action="options.php">
            <?php
                settings_fields( 'appivo_options' );
                do_settings_sections( 'appivo_authentication_section' );
                submit_button('Validate and Save');
            ?>
            </form>
        </div>
        <?php
    }

    public function add_authentication_section(){
        
        register_setting(
            'appivo_options', // Option group
            'appivo_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'appivo_authentication_section_id', // ID
            'Enter Authentication Key and Secret :', // Title
            array( $this, 'print_section_info' ), // Callback
            'appivo_authentication_section' // Page
        );

        add_settings_field(
            'user_access_token', // ID
            'User Access Token', // Title 
            array( $this, 'user_access_token_callback' ), // Callback
            'appivo_authentication_section', // Page
            'appivo_authentication_section_id' // Section           
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        //sanitize inputs to verify input makes sense
        return $input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        /*silence is golden*/
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function user_access_token_callback()
    {
        printf(
            '<input type="text" id="user_access_token" name="appivo_options[user_access_token]" value = "%s" style="width:650px; height: 35px;"/>',
            isset($this->options) && isset( $this->options['user_access_token']) && strlen($this->options['user_access_token']) > 0 ? esc_attr( $this->options['user_access_token']) : ''          
        );
    }

    /*
    * CallBack function to push appivo's settings link
    * params - links -> contains all links for plugin
    */
    public function add_appivo_settings_link($links){
        $settinslink = '<a href="options-general.php?page=appivo_plugin">Settings</a>';
        array_push($links, $settinslink);
        return $links;
    }
}
