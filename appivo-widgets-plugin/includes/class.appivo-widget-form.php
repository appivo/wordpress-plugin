<?php 

class Appivo_Widget_Form{
	const post_type = 'appivo_widgets';

	private static $found_items = 0;
	private static $current = null;

	public $id;
	public $name;
	public $title;
	public $appname;
    public $modelname;
    public $viewType;
    public $queryname;
    public $queryParams;
    public $template_type;
    public $customTemplate;
    public $appivo_isDefaultTemplate;
    public $singleViewParams;
	private $locale;
	private $properties = array();
	private $unit_tag;
	private $responses_count = 0;
	private $scanned_form_tags;
	public $shortcode_atts = array();

	public static function count() {
		return self::$found_items;
	}

	public static function getAppivoWidgets( $args = '' ) {
		$defaults = array(
			'post_status' => 'any',
			'posts_per_page' => -1,
			'offset' => 0,
			'orderby' => 'ID',
			'order' => 'ASC',
		);

		$args = wp_parse_args( $args, $defaults );

		$args['post_type'] = self::post_type;

		$q = new WP_Query();
		$posts = $q->query( $args );

		self::$found_items = $q->found_posts;
		$objs = array();

		foreach ( (array) $posts as $post ) {
			$objs[] = new self( $post );
		}

		return $objs;
	}

	public function __construct( $post = null ) {
		
		$post = get_post( $post );
		
		if ( $post && self::post_type == get_post_type( $post ) ) {
			$this->id = $post->ID;
			$this->title = $post->post_title;
			$this->appname = $post->appname;
		    $this->modelname = $post->modelname;
		    $this->viewType = $post->viewType;
		    $this->queryname = $post->queryname;
		    $this->queryParams = $post->queryname;
		    $this->template_type = $post->template_type;
		    $this->customTemplate = $post->customTemplate;
		    $this->appivo_isDefaultTemplate = $post->appivo_isDefaultTemplate;
		    $this->singleViewParams = $post->singleViewParams;
		    $this->singleViewRecordRelations = $post->singleViewRecordRelations;
		    
		}
	}

	public static function get_instance( $post ) {
		$post = get_post( $post );

		if ( ! $post || self::post_type != get_post_type( $post ) ) {
			return false;
		}

		return self::$current = new self( $post );
	}

	public function initial() {
		return empty( $this->id );
	}

	public function delete() {
		if ( $this->initial() ) {
			return;
		}

		if ( wp_delete_post( $this->id, true ) ) {
			$this->id = 0;
			return true;
		}

		return false;
	}

	public function save() {
		$isInsert = false;
		if ( $this->initial() ) {
			$post_id = wp_insert_post( array(
				'post_type' => self::post_type,
				'post_status' => 'publish',
				'post_title' => $this->title,
				'post_content' => $this->customTemplate,
			));
			$isInsert = true;
		} else {
			$post_id = wp_update_post( array(
				'ID' => (int) $this->id,
				'post_status' => 'publish',
				'post_title' => $this->title,
				'post_content' =>$this->customTemplate,
			));
		}

		if ( $post_id ) {

			update_post_meta( $post_id, 'appname', $this->appname);
			update_post_meta( $post_id, 'modelname', $this->modelname);
			update_post_meta( $post_id, 'viewType', $this->viewType);
			update_post_meta( $post_id, 'queryname', $this->queryname);
			update_post_meta( $post_id, 'queryParams', $this->queryParams);
			update_post_meta( $post_id, 'template_type', $this->template_type);
			update_post_meta( $post_id, 'appivo_isDefaultTemplate', $this->appivo_isDefaultTemplate);
			update_post_meta( $post_id, 'singleViewParams', $this->singleViewParams);
			update_post_meta( $post_id, 'singleViewRecordRelations', $this->singleViewRecordRelations);
		}

		return $post_id;
	}


}


?>