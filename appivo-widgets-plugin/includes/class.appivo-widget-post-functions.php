<?php


require_once(dirname(dirname( __FILE__ )).'/vendor/autoload.php');
use Appivo\Client\appivo_client;

    function apdw_save_contact_form( $args = '', $context = 'save' ) {
            $args = wp_parse_args( $args, array(
                'post_title' => '',
                'appname'    => '',
                'modelname'  => '',

                'viewType' => 'singleView',

                'queryname' => '',
                'queryParams' => '',
                'template_type' => 'list',
                'customTemplate' => '',
                'appivo_isDefaultTemplate' => false,
                'singleViewParams' => ''
            ));

            $args['id'] = (int) $args['id'];

            if ( -1 == $args['id'] ) {
                $widget_form = new Appivo_Widget_Form();
            } else {
                $widget_form = new Appivo_Widget_Form();
                $widget_form->id = $args['id'];
            }

            $widget_form->title = $args['post_title'];
            $widget_form->appname = $args['appname'];
            $widget_form->modelname = $args['modelname'];
            $widget_form->viewType = $args['viewType'];
            $widget_form->queryname = $args['queryname'];
            $widget_form->queryParams = $args['queryParams'];
            $widget_form->template_type = $args['template_type'];
            $widget_form->customTemplate = $args['customTemplate'];
            $widget_form->appivo_isDefaultTemplate = $args['appivo_isDefaultTemplate'];
            $widget_form->singleViewParams = $args['singleViewParams'];
            $widget_form->singleViewRecordRelations = $args['singleViewRecordRelations'];

            if ( empty( $widget_form ) ) {
                return false;
            }

            // do_action( 'wpcf7_save_widget_form', $widget_form, $args, $context );

            if ( 'save' == $context ) {
                $widget_form->save();
            }

            return $widget_form;
        
    }

    function apdw_cf7_before_send_mail( $WPCF7_ContactForm ) {
        $submission = WPCF7_Submission::get_instance();
        $wpcf = WPCF7_ContactForm::get_current();

        $submited_data = $submission->get_posted_data();

        $record = array();

        if (isset($submited_data['isAppivoForm']) && $submited_data['isAppivoForm'] == true) {

            $options = get_option('appivo_options');
            if (isset($options) && isset($options['user_access_token'])) {
                $user_access_token = $options['user_access_token'];
            } else {
                throw new Exception("User token not set", 1);
            }

            $appivo_client = new appivo_client($user_access_token);

            if (isset($submited_data['isTenantCreationForm']) && $submited_data['isTenantCreationForm'] == true) {
                apdw_process_tenant_creation($submited_data, $appivo_client);
            } else {
                if (! (isset($submited_data['appivo_app-name']) && strlen($submited_data['appivo_app-name']) > 0)) {
                    throw new \InvalidArgumentException('App Name is required');
                }

                foreach ($submited_data as $key => $value)
                {
                    //Check if incoming parameter is an appivo form parameter
                    if ((strpos($key, 'appivo_param_') === 0)) {
                        $splitKey = explode("appivo_param_", $key);
                        $modifiedKey = $splitKey[1];
                        $record[$modifiedKey] = $value;
                    }
                }


                if (isset($submited_data['appivo_service-name']) && strlen($submited_data['appivo_service-name']) > 0) {
                    $response = $appivo_client->invokeService($submited_data['appivo_app-name'], $submited_data['appivo_service-name'], 'POST', $record);
                }
                else {
                    if (! (isset($submited_data['appivo_model-name']) && strlen($submited_data['appivo_model-name']) > 0)) {
                        throw new \InvalidArgumentException('Model Name is required');
                    }
                    $responseCreate = $appivo_client->createRecord($submited_data['appivo_app-name'],$submited_data['appivo_model-name'], $record);
                }
            }
        }

        //return $wpcf;

    }

    function apdw_process_tenant_creation ($submited_data, $users = array(), $appivo_client, $configuration = array()) {
        
        $maxRetries = 3;
        $attempts = 0;
        do {
            try {

                if (!(isset($submited_data['appivo_tenant_name'])  && strlen($submited_data['appivo_tenant_name']) > 0)){
                    throw new \InvalidArgumentException('Tenant name is required');
                }

                $tenant = array();

                $tenant["name"] = $submited_data['appivo_tenant_name'];
                $tenant["domain"] = isset($submited_data['appivo_tenant_domain']) ? $submited_data['appivo_tenant_domain']: null;

                //TimeZone ex: Austrailia/Brisbane
                $tenant["timezone"]= isset($submited_data['appivo_tenant_timezone']) ? $submited_data['appivo_tenant_timezone']: null;
                //Country ex: AU
                $tenant["country"]= isset($submited_data['appivo_tenant_country']) ? $submited_data['appivo_tenant_country']: null;


                $tenantDetails = array();
                $tenantDetails["address1"] = isset($submited_data['appivo_tenant_address1']) ? $submited_data['appivo_tenant_address1'] : '';
                $tenantDetails["address2"] = isset($submited_data['appivo_tenant_address2']) ? $submited_data['appivo_tenant_address2'] : '';
                $tenantDetails["city"] = isset($submited_data['appivo_tenant_city']) ? $submited_data['appivo_tenant_city'] : '';
                $tenantDetails["zipcode"] = isset($submited_data['zipcode']) ? $submited_data['zipcode'] : '';
                $tenantDetails["phone"] = isset($submited_data['appivo_tenant_phone']) ? $submited_data['appivo_tenant_phone'] : '';
                $tenantDetails["invoiceaddress1"] = isset($submited_data['appivo_tenant_invoiceaddress1']) ? $submited_data['appivo_tenant_invoiceaddress1'] : '';
                $tenantDetails["invoiceaddress2"] = isset($submited_data['appivo_tenant_invoiceaddress2']) ? $submited_data['appivo_tenant_invoiceaddress2'] : '';
                $tenantDetails["invoicezipcode"] = isset($submited_data['appivo_tenant_invoicezipcode']) ? $submited_data['appivo_tenant_invoicezipcode'] : '';

                $tenantDetails["billing_email"] = isset($submited_data['appivo_tenant_billing_email']) ? $submited_data['appivo_tenant_billing_email'] : '';
                $tenantDetails["vatid"] = isset($submited_data['vatid']) ? $submited_data['vatid'] : '';

                $tenantData = array();
                foreach ($submited_data as $key => $value)
                {
                    if ((strpos($key, 'appivo_tenant_data_') === 0)) {
                        $splitKey = explode("appivo_tenant_data_", $key);
                        $modifiedKey = $splitKey[1];
                        $tenantData[$modifiedKey] = $value;
                    }
                }

                $metaData = array();

                $subscription = array("app"=>$submited_data['appivo_tenant_app_name'], "features"=>$submited_data['appivo_tenant_features'], "config" => $configuration);

                $country = isset($submited_data['appivo_tenant_country']) ? $submited_data['appivo_tenant_country'] : '';

                $tenantObj = array("tenant"=>$tenant, "data"=> $tenantData, "tenantDetails" => $tenantDetails, "users" => $users, "subscription" => $subscription, "config" => array(), "country" => $country, "inheritWhitelabel" => $submited_data['appivo_tenant_copyWhitelabel']);


                $responseCreate = $appivo_client->createTenant($tenantObj);
                return $responseCreate;
            }
            catch (\Exception $ex) {
                $attempts++;
                if ($attempts == $maxRetries) {
                    throw $ex;
                } else {
                    usleep(300000); //300*1000 microseconds
                    continue;
                }
            }
            break;
        } while ($attempts < $maxRetries);
        
    }

    function apdw_get_appivo_client($host = 'apps.appivo.com') {
        $options = get_option('appivo_options');
            if (isset($options) && isset($options['user_access_token'])) {
                $user_access_token = $options['user_access_token'];
            } else {
                throw new Exception("User token not set", 1);
            }

            $appivo_client = new appivo_client($user_access_token, $host);
            return $appivo_client;
    }


?>