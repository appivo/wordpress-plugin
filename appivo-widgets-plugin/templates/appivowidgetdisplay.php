<?php 

//Use composer autoloader

require_once(dirname(dirname( __FILE__ )).'/vendor/autoload.php');

use LightnCandy\LightnCandy;
use Appivo\Client\appivo_client;


class Appivo_Widget_Display {

	public $optionsAppivo;
	private $key;
	private $secret;

	public function __construct(){
		$this->optionsAppivo = get_option('appivo_options');

		if (empty($this->optionsAppivo['user_key']) || empty($this->optionsAppivo['user_secret']) ) {
			throw new Exception("User Key and Secret not set in Appivo Settings", 1);
		} else {
			$this->key = $this->optionsAppivo['user_key'];
			$this->secret = $this->optionsAppivo['user_secret'];
		}	
	}

	public function getWidgetHtml($instance = null){

		if (empty($instance)) {
			echo 'Nothing to show. Check your post settings again';
		}

		// Check the widget options

		$appname    = isset( $instance['appname'] ) ? $instance['appname'] : '';
		$viewType   = isset( $instance['viewType'] ) ? $instance['viewType'] : '';
		$modelname     = isset( $instance['modelname'] ) ? $instance['modelname'] : '';

		$queryname = isset( $instance['queryname'] ) ? $instance['queryname'] : '';
		$queryParams = isset( $instance['queryParams'] ) ? $instance['queryParams'] : '';


		$template_type   = isset( $instance['template_type'] ) ? $instance['template_type'] : '';
		$appivo_isDefaultTemplate = ! empty( $instance['appivo_isDefaultTemplate']) ? $instance['appivo_isDefaultTemplate'] : false;

		$customTemplate = isset( $instance['customTemplate'] ) ? $instance['customTemplate'] : '';
		$singleViewParams = isset( $instance['singleViewParams'] ) ? $instance['singleViewParams'] : '';
		$singleViewRecordRelations = isset( $instance['singleViewRecordRelations'] ) ? $instance['singleViewRecordRelations'] : '';
		

		//section pertaining logic of query view

		if (($viewType == 'queryView')) {
			$data = $this->getDataFromQuery($appname, $queryname, $this->generateParamsForQuery($queryParams), $modelname);
		}

		if (($viewType == 'singleView')){

			$idval = '';
			if (strlen($singleViewParams) > 0){
				$idParam = json_decode($singleViewParams);
				switch ($idParam->valuesource) {
					    case 'userInput':
					        $idval = $idParam->value;
					        break;
					    case 'url':
					    	if (isset($_GET[$idParam->value])) {
					    		$idval = $_GET[$idParam->value];
					    	}
					    	else {
					    		throw new Exception("No Query Parameter found in Url", 1);
					    	}
					        break;
					    case 'cookie':
					        //Cookie logic goes here
					        break;
					    default:
					}
			}

			if (strlen($singleViewRecordRelations) > 0){
				$data = $this->getSingleRecord($appname, $modelname, $idval, $singleViewRecordRelations);
			} else {
				$data = $this->getSingleRecord($appname, $modelname, $idval);
			}
			
			
		}

		$appivo_meta_data = array("wordpressUrl" => get_site_url());

		if ($data) {
			wp_localize_script( 'appivoclientjs', 'appivo_widget_data_result', $data );
			wp_localize_script( 'appivoclientjs', 'appivo_widget_template', $customTemplate );
			wp_localize_script( 'appivoclientjs', 'appivo_widget_meta_data', json_encode($appivo_meta_data));

			$pageStr = '<div class="appivo-widget-html"></div>';
			
			return $pageStr;
		}
	}


	/*
	* Generates Parameters to be used in appivo calls to query from different listed sources
	* Arguments: query paramets in JSON format
	* Return: phpArray of finalQueryParameters that can be directly used in appivo calls
	*/
	private function generateParamsForQuery($queryParamsJson) {
		$finalQueryParamArray = array();
		if (strlen($queryParamsJson) > 0){
			$queryParamsArray = json_decode($queryParamsJson);
			if (count($queryParamsArray) > 0) {
				foreach($queryParamsArray as $item) {
					switch ($item->valuesource) {
					    case 'userInput':
					        $finalQueryParamArray[$item->key] = $item->value;
					        break;
					    case 'url':
					    	if (isset($_GET[$item->value])) {
					    		$finalQueryParamArray[$item->key] = $_GET[$item->value];
					    	}
					    	else {
					    		throw new Exception("No Query Parameter found in Url", 1);
					    	}
					        break;
					    case 'cookie':
					        //Cookie logic goes here
					        break;
					    default:
					}
				}
				unset($item);
			}
			
		}
		return $finalQueryParamArray;
	}


	//----------------------------------------------------------Section of Appivo calls ------------------------------------------------------

	function getDataFromQuery($appname, $queryname = '', $queryParams, $modelname = ''){
		$appivo_client = new appivo_client($this->key, $this->secret);

		if (empty($queryname)) {
			$responseGetQuery = $appivo_client->getAllRecords($appname, $modelname);
		}
		else {
			$responseGetQuery = $appivo_client->executeQuery($appname, $queryname, $queryParams);
		}

		$response = $this->objectToArray($responseGetQuery);
		return $response;
	}

	function getSingleRecord($appname, $modelname, $id, $relations = '') {

		$appivo_client = new appivo_client($this->key, $this->secret);

		if (strlen($relations) > 0){
			$customheaders = array('relations' => $relations);
			$responseGetQuery = $appivo_client->getRecord($appname, $modelname, $id, $customheaders);
		} else {
			$responseGetQuery = $appivo_client->getRecord($appname, $modelname, $id);
		}
		

		$response = $this->objectToArray($responseGetQuery);
		return $response;
	}

	//----------------------------------------------------------End of Appivo calls ---------------------------------------------------------


	function objectToArray($d) {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }
		
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(__METHOD__, $d);
        }
        else {
            // Return array
            return $d;
        }
    }

}

?>